import datetime
from typing import cast, Dict, Hashable, List, NoReturn, Tuple

# Note: we use the linux util (even though we're on Windows; very handy) "sort" to convert the
#  original input file to a sorted input file, since the data is structured and the timestamp is in most-significant-
#  -first order and is zero-padded where appropriate, meaning lex sort and numeric sort are the same.
#  So our "real_input.txt" is already in order.

# Note: we use grep to verify there are no events in the input file which happen outside the midnight hour and are NOT
#  shift-starts.
# 1. grep -v " 00:" real_input.txt
# 2. grep -v " 00:" real_input.txt | grep -v "shift"
# Result: we definitely saw rows not containing " 00:" (which can only be the hour in the timestamp), and then
#  we definitely saw no rows among those lacking the word "shift".  So we can be confident that all events outside
#  the midnight hour are only shift-starts.


class Histogram(object):
    def __init__(self):
        self.data = dict()  # type: Dict[Hashable, int]

    def increment(self, key: Hashable):
        if key not in self.data:
            self.data[key] = 1
        else:
            self.data[key] += 1

    def get_most_popular_key(self) -> Hashable:
        """ Return the single key which has the largest value.  Raise if tied. """
        keys_sorted_by_values = sorted(self.data.keys(), key=lambda k: self.data[k])

        # case not handled: no elements.
        if len(keys_sorted_by_values) < 2:
            return keys_sorted_by_values[0]

        ultimate = keys_sorted_by_values[-1]
        penultimate = keys_sorted_by_values[-2]
        if self.data[ultimate] == self.data[penultimate]:
            raise Exception(f'Dict greatest-value tie (keys {ultimate} and {penultimate}, at least).')
        return ultimate


class InputEvent(object):
    """ Contain any/every input file line in prepared format """
    def __init__(self, date: datetime.date, minute_number: int, event_type: str, guard_num: int):
        self.date = date
        self.minute_number = minute_number
        self.is_fall_asleep = event_type == 'f'
        self.is_wake_up = event_type == 'w'
        self.is_shift_start = event_type == 'G'
        self.guard_num = guard_num
        # minor sanity check
        if not self.is_shift_start and self.guard_num > 0:
            fail_msg = f'Event type "{event_type}" was not expected to have a positive guard number ({guard_num}).'
            raise Exception(fail_msg)

    def __str__(self):
        event_descr = f'Shift_start {self.guard_num}' if self.is_shift_start \
            else 'Wake_up' if self.is_wake_up \
            else 'Fall_asleep'

        return f'InputEvent: {self.date}, minute {self.minute_number}, {event_descr}'

    @staticmethod
    def parse_from_line(in_line: str) -> "InputEvent":
        # all cases of sample data:
        # [1518-03-16 00:04] Guard #1973 begins shift
        # [1518-03-16 00:34] falls asleep
        # [1518-03-16 00:39] wakes up
        line = in_line.strip()
        year = int(line[1:5])
        month = int(line[6:8])
        day = int(line[9:11])
        # We need to keep dates unique and, probably, repeatable.  We also need an easy way to add 1 to a date, because
        #  we're going to roll over pre-midnight shift starts to 00:00 on the following day.  There's no easy shortcut
        #  to doing that other than parsing the date into reality, since we might have a shift start on the last
        #  day of one month and some related events happen on the first day of the following month.
        date = datetime.date(year, month, day)
        hour = int(line[11:13])
        minute = int(line[15:17])
        event_type = line[19:20]
        guard_num = int(line[26:30]) if event_type == 'G' else -1

        # Assumption, checked on command line: no guard number is negative or zero.
        if guard_num > 0:
            # if the hour is not the midnight hour, adjust it to be 00:00 on the next day, so our timekeeping later
            #  is simplified.
            if hour > 0:
                hour = 0
                minute = 0
                date = date + datetime.timedelta(days=1)

        return InputEvent(date, minute, event_type, guard_num)


class SleepingMinute(object):
    """ The core data we actually need to track and aggregate (i.e. count) """
    def __init__(self, guard_num: int, minute_num: int):
        self.guard_num = guard_num
        self.minute_num = minute_num


class HistoryEngine(object):
    def __init__(self, in_file_path: str):
        self.events = [InputEvent.parse_from_line(line.strip()) for line in open(in_file_path, 'r').readlines()]
        self.sleeping_minutes = []  # type: List[SleepingMinute]
        self._make_sleeping_minutes()

    def _make_sleeping_minutes(self) -> NoReturn:
        cur_guard = -1
        # If the input is explicitly correct and we read it right, we don't need to track this.  But this can help
        #  us sanity-check how we're interpreting/reading the input.
        is_asleep = False
        fell_asleep_minute = -1
        for event in self.events:
            if event.is_shift_start:
                if is_asleep:
                    raise Exception(f'Unexpected: Guard {cur_guard} did not wake up before end of shift.')
                cur_guard = event.guard_num
            elif event.is_fall_asleep:
                if is_asleep:
                    print(event)
                    raise Exception('Got to a fall_asleep event but already thought the current guard was asleep.')
                fell_asleep_minute = event.minute_number
                is_asleep = True
            elif event.is_wake_up:
                if not is_asleep:
                    raise Exception('Got to a wakeup event without a corresponding prior fall_asleep event.')
                wake_minute = event.minute_number
                for minute_counter in range(fell_asleep_minute, wake_minute):
                    self.sleeping_minutes.append(SleepingMinute(cur_guard, minute_counter))
                is_asleep = False
            else:
                raise Exception(f'Unexpected / no branch for event type: {event}')

    def get_sleepiest_guard(self) -> int:
        """
        Get information about the single guard who was asleep the most.
        :return: guard_num of guard with the most minutes asleep.  Raise if there is a tie.
        """
        guard_sleep_histo = Histogram()
        for sleeping_minute in self.sleeping_minutes:
            guard_sleep_histo.increment(sleeping_minute.guard_num)
        return cast(int, guard_sleep_histo.get_most_popular_key())

    def get_most_common_minute_asleep(self, search_guard_num: int) -> int:
        minute_sleep_histo = Histogram()
        for sleeping_minute in self.sleeping_minutes:
            if sleeping_minute.guard_num != search_guard_num:
                continue
            minute_sleep_histo.increment(sleeping_minute.minute_num)
        return cast(int, minute_sleep_histo.get_most_popular_key())


    def get_sleepiest_guard_minute_combination(self) -> Tuple[int, int]:
        """
        Get information about the guard-minute sleep combination which appeared the most times, i.e. the guard
         who was asleep the largest number of times on any single minute.
        :return: 2-tuple of guard_num and minute_num of the most-popular intersection.  Raise if there is a tie.
        """
        sleepiest_guard_minute_histo = Histogram()
        for sleeping_minute in self.sleeping_minutes:
            sleepiest_guard_minute_histo.increment((sleeping_minute.guard_num, sleeping_minute.minute_num))
        return cast(Tuple[int, int], sleepiest_guard_minute_histo.get_most_popular_key())


def run_main_part_1() -> NoReturn:
    """
    Part 1: find the ID of the guard who sleeps the most total minutes, and find the mode minute that guard is asleep,
     and print the product of those two numbers.
    """
    engine = HistoryEngine('real_input.txt')
    sleepiest_guard_num = engine.get_sleepiest_guard()
    print(f'Sleepiest guard\'s ID is {sleepiest_guard_num}.')
    mode_sleeping_minute = engine.get_most_common_minute_asleep(sleepiest_guard_num)
    print(f'That guard was asleep most often at minute {mode_sleeping_minute}.')
    print(f'The product of those two numbers is {sleepiest_guard_num * mode_sleeping_minute}.')
    # Output:
    # Sleepiest guard's ID is 641.
    # That guard was asleep most often at minute 41.
    # The product of those two numbers is 26281.  // This is the correct answer for part 1.


def run_main_part_2() -> NoReturn:
    """
    Part 2: find the guard-minute combination which has the highest being-asleep count.
    Print the product of that guard's ID and the minute number.
    """
    engine = HistoryEngine('real_input.txt')
    guard_num, minute_num = engine.get_sleepiest_guard_minute_combination()
    print(f'Sleepiest guard-minute is guard ID {guard_num}, minute {minute_num}.')
    print(f'The product of those two numbers is {guard_num * minute_num}.')
    # Output:
    # Sleepiest guard-minute is guard ID 1973, minute 37.
    # The product of those two numbers is 73001.  // This is the correct answer for part 2.

if __name__ == '__main__':
    print('Part 1:')
    run_main_part_1()
    print('-=-=-=-=-')
    print('Part 2:')
    run_main_part_2()
