#input_file_handle = open("sample_input_2.txt", "r")
#input_file_handle = open("sample_input_3.txt", "r")
#input_file_handle = open("sample_input_5.txt", "r")
input_file_handle = open("real_input.txt", "r")
lines = input_file_handle.readlines()
#print(lines)

my_total = 0

seen_numbers = set()
seen_numbers.add(my_total)

while True:
    for line in lines:
        stripped_line = line.strip()
        this_line_int = int(stripped_line)
        my_total = my_total + this_line_int
        
        number_has_been_seen = my_total in seen_numbers
        if number_has_been_seen:
            print("The value " + str(my_total) + " has been seen before.")
            exit()
            
        seen_numbers.add(my_total)
        
        #print(my_total)
