from typing import List, Optional, Tuple

# DIRECTIONS
# "NOWHERE" is a valid move which means "this room" in a path; it's used to make a placeholder node (e.g. linked-list head).
NOWHERE = ''
NORTH = 'N'
EAST = 'E'
SOUTH = 'S'
WEST = 'W'

# Indexes of neighbors
NEIGHBOR_NORTH = 0
NEIGHBOR_EAST = 1
NEIGHBOR_SOUTH = 2
NEIGHBOR_WEST = 3

REVERSE_NEIGHBOR_INDEX_DIRECTIONS = [NORTH, EAST, SOUTH, WEST]


STEP_IN = '('  # This means "append all of the following subpaths to the current path(s), once we're done defining them"
START_NEW_SUBPATH = '|'  # This means "start a new subpath in the current group"
STEP_OUT = ')'  # This means "All subpaths in the current group are finalized and should be appended to the parent path(s)."
START = '^'
END = '$'


def get_neighbor_index(compass_direction: str) -> int:
    if compass_direction == NORTH:
        return 0
    elif compass_direction == EAST:
        return 1
    elif compass_direction == SOUTH:
        return 2
    elif compass_direction == WEST:
        return 3
    else:
        raise Exception(f'Programming error: invalid compass_direction "{compass_direction}".')


def anti(compass_direction: str) -> str:
    if compass_direction == NORTH:
        return SOUTH
    elif compass_direction == EAST:
        return WEST
    elif compass_direction == SOUTH:
        return NORTH
    elif compass_direction == WEST:
        return EAST
    else:
        raise Exception(f'Programming error: invalid compass_direction "{compass_direction}".')


class EndException(Exception):
    pass


def get_new_coordinate(compass_direction: str, cur_x: int, cur_y: int) -> Tuple[int, int]:
    if compass_direction == NORTH:
        return cur_x, cur_y - 1
    elif compass_direction == EAST:
        return cur_x + 1, cur_y
    elif compass_direction == SOUTH:
        return cur_x, cur_y + 1
    elif compass_direction == WEST:
        return cur_x - 1, cur_y
    else:
        raise Exception(f'Programming error: invalid compass_direction "{compass_direction}".')


class InputParser(object):
    def __init__(self, input_path_name: str):
        self.characters = self._load(input_path_name)
        self.cur_position = -1
        self.max_position = len(self.characters) - 1

    def get_next(self) -> str:
        if self.cur_position == self.max_position:
            raise EndException()
        self.cur_position += 1
        return self.characters[self.cur_position]

    def _load(self, input_path_name: str) -> str:
        #line = open(input_path_name, 'r').read().strip()
        line = open(input_path_name, 'r').readline().strip()
        assert(line[0] == START)
        assert(line[-1] == END)
        for chara in line[1:-1]:
            assert chara in (STEP_IN, START_NEW_SUBPATH, STEP_OUT, NORTH, EAST, SOUTH, WEST)
        return line[1:-1]


class Room(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.center_mark = '.'
        self.neighbors = [None, None, None, None]  # Type: List[Optional[Room]]


def print_visual_map(rooms: List[Room]):
    # Make a grid, place the rooms in the grid, then render the rooms in left-to-right, top-to-bottom grid order
    #  so they can be printed.
    # Grid sizing: room centers are on even rows and columns; walls adjoining them are on odd rows and columns;
    #  number of rows and columns is always 2x number of rooms down/across, plus 1 more.
    min_x = min([room.x for room in rooms])
    max_x = max([room.x for room in rooms])
    min_y = min([room.y for room in rooms])
    max_y = max([room.y for room in rooms])
    width = max_x - min_x
    height = max_y - min_y

    rows = [['#'] * (width * 2 + 3) for y in range(height * 2 + 3)]
    for y in range(1, (max_y - min_y) * 2 + 2, 2):
        for x in range(1, (max_x - min_x) * 2 + 2, 2):
            rows[y][x] = ' '  # mark room as completely unmarked on the map.

    for room in rooms:
        visual_center_x = (room.x - min_x) * 2 + 1
        visual_center_y = (room.y - min_y) * 2 + 1

        for neighbor_direction, neighbor in enumerate(room.neighbors):
            if neighbor:
                door_x, door_y = get_new_coordinate(
                    REVERSE_NEIGHBOR_INDEX_DIRECTIONS[neighbor_direction], visual_center_x, visual_center_y)
                door_chara = '-' if door_x == visual_center_x else '|'
                rows[door_y][door_x] = door_chara
            rows[visual_center_y][visual_center_x] = room.center_mark

    for row in rows:
        print(''.join(row))

# This implementation generates the right answer for all smaller input files, but it can't handle the real input file
#  because there's too much multiplication, resulting in out-of-memory.  So the parsing logic is correct, but
#  we can't flatten to all the individual paths because there are too many to store.  So we need to change to updating
#  the map as we go.
def expand_parenthetical(parser: InputParser, parent_chain_group: List[str]) -> List[str]:
    current_chain_group = []
    building_chains = ['']
    while True:
        try:
            next_instruction = parser.get_next()
            if next_instruction in (NORTH, EAST, SOUTH, WEST):
                building_chains = [chain + next_instruction for chain in building_chains]
            elif next_instruction == STEP_IN:
                if building_chains:
                    building_chains = expand_parenthetical(parser, building_chains)
            elif next_instruction == START_NEW_SUBPATH:
                current_chain_group.extend(building_chains)
                building_chains = ['']
            elif next_instruction == STEP_OUT:
                mirved_parent_chains = []
                for parent_chain in parent_chain_group:
                    for sub_chain in current_chain_group + building_chains:
                        mirved_parent_chains.append(parent_chain + sub_chain)
                return mirved_parent_chains
        except EndException:
            return building_chains


def run_main():
    input_file = 'real_input.txt'
    #input_file = 'sample_input_1.txt'
    #input_file = 'sample_input_2.txt'
    #input_file = 'sample_input_4.txt'
    #input_file ='my_sample_1.txt'
    parser = InputParser(input_file)
    #print(open(input_file, 'r').readline())
    #chains = build(parser)
    chains = expand_parenthetical(parser, [''])
    #print(f'There are {len(chains)} individual chains.')
    #print('Their lengths are:')
    #for chain in chains:
    #    print(len(chain))
    for chain in chains:
        chain_str = ''.join(chain)
        print(f'"{chain_str}"')
    #exit()

    center_room = Room(0, 0)
    center_room.center_mark = 'X'
    all_rooms = [center_room]
    for chain in chains:
        cur_room = center_room
        for move in chain:
            if move == '':
                continue
            neighbor_index = get_neighbor_index(move)
            if cur_room.neighbors[neighbor_index] is None:
                new_room = Room(*get_new_coordinate(move, cur_room.x, cur_room.y))
                all_rooms.append(new_room)
                new_room.neighbors[get_neighbor_index(anti(move))] = cur_room
                cur_room.neighbors[neighbor_index] = new_room
            cur_room = cur_room.neighbors[neighbor_index]

    print_visual_map(all_rooms)


if __name__ == '__main__':
    run_main()
