from collections import deque
from typing import List, NoReturn, Optional, Tuple

# DIRECTIONS
NORTH = 'N'
EAST = 'E'
SOUTH = 'S'
WEST = 'W'

# Indexes of neighbors
NEIGHBOR_NORTH = 0
NEIGHBOR_EAST = 1
NEIGHBOR_SOUTH = 2
NEIGHBOR_WEST = 3

STEP_IN = '('  # This means "append all of the following subpaths to the current path(s), once we're done defining them"
START_NEW_SUBPATH = '|'  # This means "start a new subpath in the current group"
STEP_OUT = ')'  # This means "All subpaths in the current group are finalized and should be appended to the parent path(s)."
START = '^'
END = '$'


def get_neighbor_index(compass_direction: str) -> int:
    if compass_direction == NORTH:
        return 0
    elif compass_direction == EAST:
        return 1
    elif compass_direction == SOUTH:
        return 2
    elif compass_direction == WEST:
        return 3
    else:
        raise Exception(f'Programming error: invalid compass_direction "{compass_direction}".')


def anti(compass_direction: str) -> str:
    if compass_direction == NORTH:
        return SOUTH
    elif compass_direction == EAST:
        return WEST
    elif compass_direction == SOUTH:
        return NORTH
    elif compass_direction == WEST:
        return EAST
    else:
        raise Exception(f'Programming error: invalid compass_direction "{compass_direction}".')


class EndException(Exception):
    pass


def get_new_coordinate(compass_direction: str, cur_x: int, cur_y: int) -> Tuple[int, int]:
    if compass_direction == NORTH:
        return cur_x, cur_y - 1
    elif compass_direction == EAST:
        return cur_x + 1, cur_y
    elif compass_direction == SOUTH:
        return cur_x, cur_y + 1
    elif compass_direction == WEST:
        return cur_x - 1, cur_y
    else:
        raise Exception(f'Programming error: invalid compass_direction "{compass_direction}".')


class InputParser(object):
    def __init__(self, input_path_name: str):
        self.characters = self._load(input_path_name)
        self.cur_position = -1
        self.max_position = len(self.characters) - 1

    def get_next(self) -> str:
        if self.cur_position == self.max_position:
            raise EndException()
        self.cur_position += 1
        return self.characters[self.cur_position]

    def _load(self, input_path_name: str) -> str:
        #line = open(input_path_name, 'r').read().strip()
        line = open(input_path_name, 'r').readline().strip()
        assert(line[0] == START)
        assert(line[-1] == END)
        for chara in line[1:-1]:
            assert chara in (STEP_IN, START_NEW_SUBPATH, STEP_OUT, NORTH, EAST, SOUTH, WEST)
        return line[1:-1]


class Room(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.center_mark = '.'
        self._neighbors = [None, None, None, None]  # Type: List[Optional[Room]]

    @property
    def coord(self) -> Tuple[int, int]:
        return self.x, self.y

    def set_neighbor(self, direction: str, existing_room: 'Room') -> NoReturn:
        self._neighbors[get_neighbor_index(direction)] = existing_room

    @property
    def all_nonempty_neighbors(self) -> List['Room']:
        """ List of neighbors, with no direction information. """
        return [n for n in self._neighbors if n]

    @property
    def directed_neighbors(self) -> List[Tuple[str, 'Room']]:
        return_value = []
        for neighbor_dir, compass_dir in (
                (NEIGHBOR_NORTH, NORTH),
                (NEIGHBOR_EAST, EAST),
                (NEIGHBOR_SOUTH, SOUTH),
                (NEIGHBOR_WEST, WEST)):
            if self._neighbors[neighbor_dir]:
                return_value.append((compass_dir, self._neighbors[neighbor_dir]))
        return return_value

    def get_or_create_neighbor(self, direction: str) -> 'Room':
        """
        Add a new neighbor in the given direction and return it - unless there's already a neighbor in that direction,
         in which case just return the existing one.
        """
        index = get_neighbor_index(direction)
        if self._neighbors[index]:
            return self._neighbors[index]
        new_neighbor = Room(*get_new_coordinate(direction, self.x, self.y))
        new_neighbor.set_neighbor(anti(direction), self)
        self._neighbors[index] = new_neighbor
        return new_neighbor

    def __repr__(self) -> str:
        neighbors_string_sequence = ' '.join(['.' if neighbor is None else 'x' for neighbor in self._neighbors])
        return f'({self.x}, {self.y}) - [{neighbors_string_sequence}]'


def print_visual_map(rooms: List[Room]):
    # Make a grid, place the rooms in the grid, then render the rooms in left-to-right, top-to-bottom grid order
    #  so they can be printed.
    # Grid sizing: room centers are on even rows and columns; walls adjoining them are on odd rows and columns;
    #  number of rows and columns is always 2x number of rooms down/across, plus 1 more.
    min_x = min([room.x for room in rooms])
    max_x = max([room.x for room in rooms])
    min_y = min([room.y for room in rooms])
    max_y = max([room.y for room in rooms])
    width = max_x - min_x
    height = max_y - min_y

    rows = [['#'] * (width * 2 + 3) for y in range(height * 2 + 3)]
    for y in range(1, (max_y - min_y) * 2 + 2, 2):
        for x in range(1, (max_x - min_x) * 2 + 2, 2):
            rows[y][x] = ' '  # mark room as completely unmarked on the map.

    for room in rooms:
        visual_center_x = (room.x - min_x) * 2 + 1
        visual_center_y = (room.y - min_y) * 2 + 1

        for neighbor_direction, neighbor in room.directed_neighbors:
            if neighbor:
                door_x, door_y = get_new_coordinate(neighbor_direction, visual_center_x, visual_center_y)
                door_chara = '-' if door_x == visual_center_x else '|'
                rows[door_y][door_x] = door_chara
            rows[visual_center_y][visual_center_x] = room.center_mark

    for row in rows:
        print(''.join(row))


def determine_distance_all_rooms(root: Room) -> List[Tuple[int, List[Room]]]:
    #return_value = [(0, [root])]
    return_value = deque()
    return_value.append((0, [root]))
    already_measured_rooms = set()
    already_measured_rooms.add(root)
    last_distance = 0

    cur_neighbors = set()
    num_measured = 0
    while True:
        cur_distance = last_distance + 1
        if cur_distance % 100 == 0:
            print(f'Processing distances... cur distance = {cur_distance}; {num_measured} total rooms measured.')
        last_step_rooms = return_value[-1][1]
        cur_neighbors.clear()
        for last_step_room in last_step_rooms:
            #print(last_step_room)
            cur_neighbors.update([n for n in last_step_room.all_nonempty_neighbors if n not in already_measured_rooms])
        #print(f'Current set of neighbors: {cur_neighbors}')
        if not cur_neighbors:
            return return_value
        return_value.append((cur_distance, list(cur_neighbors)))
        already_measured_rooms.update(cur_neighbors)
        num_measured += len(cur_neighbors)
        last_distance = cur_distance


def process_input(parser: InputParser) -> Room:
    root = Room(0, 0)
    root.center_mark = 'X'
    # Current ends is -1; Parent ends is -2
    ends_stack = [[root], [root]]
    while True:
        try:
            next_instruction = parser.get_next()
            if next_instruction in (NORTH, EAST, SOUTH, WEST):
                new_ends = [room.get_or_create_neighbor(next_instruction) for room in ends_stack[-1]]
                ends_stack[-1] = new_ends
            elif next_instruction == STEP_IN:
                # push a copy of the current working ends onto the end of the stack when we step_in; this will be the
                #  new set of working ends until we step out.
                ends_stack.append(list(ends_stack[-1]))
            elif next_instruction == START_NEW_SUBPATH:
                # reset the working ends to the parent ends, so our next additions can work off of them.
                ends_stack[-1] = ends_stack[-2]
            elif next_instruction == STEP_OUT:
                ends_stack.pop()
        except EndException:
            # TODO: cleanup of anything?  Or are we all current?
            return root


def run_main():
    input_file = 'real_input.txt'
    #input_file = 'sample_input_1.txt'
    #input_file = 'sample_input_2.txt'
    #input_file = 'sample_input_4.txt'
    #input_file ='my_sample_1.txt'
    parser = InputParser(input_file)
    #print(open(input_file, 'r').readline())
    #chains = build(parser)
    #chains = expand_parenthetical(parser, [''])
    root = process_input(parser)
    #print(f'There are {len(chains)} individual chains.')
    #print('Their lengths are:')
    #for chain in chains:
    #    print(len(chain))
    # for chain in chains:
    #     chain_str = ''.join(chain)
    #     print(f'"{chain_str}"')

    counter = 0

    printed_room_coords = set()
    all_rooms = []  # type: List[Room]

    def print_room(a_room: Room) -> NoReturn:
        pass
        #print(f'Room {counter}: ({a_room.x}, {a_room.y})')

    def find_all_room_and_children(a_room: Room) -> NoReturn:
        nonlocal counter
        end_found = False
        while not end_found:
            print_room(a_room)
            counter += 1
            printed_room_coords.add(a_room.coord)
            all_rooms.append(a_room)

            neighbors = [room for room in a_room.all_nonempty_neighbors if room.coord not in printed_room_coords]
            if len(neighbors) == 0:
                end_found = True
            elif len(neighbors) == 1:
                a_room = neighbors[0]
            else:
                for child in neighbors:
                    find_all_room_and_children(child)
                end_found = True  # we used recursion to handle all the children, so we end our loop in this func.

    #find_all_room_and_children(root)
    rooms_by_distance = determine_distance_all_rooms(root)
    #print(f'{len(all_rooms)} rooms total.')
    print(f'Rooms by distance: ')
    for distance, rooms in rooms_by_distance:
        print(f'{len(rooms)} rooms have distance {distance} doors away from start.')

    # My first guess will be 3314, even though my calculations show two different rooms which are that distance away. :/
    #  Great news - that is the correct answer. :)

    # Part 2: count how many rooms are >= 1000 doors away.
    num_rooms_at_least_1k_away = 0
    for distance, rooms in rooms_by_distance:
        if distance >= 1000:
            num_rooms_at_least_1k_away += len(rooms)
    print(f'{num_rooms_at_least_1k_away} rooms are at least 1,000 doors away from the start.')
    # My first guess will be 8,550.
    #  Woo, it was correct!

    #print_visual_map(all_rooms)



if __name__ == '__main__':
    run_main()
