Processing rooms as we go:
* until we find an open-paren, we can just build on the current room-chain.
  Q: how do we start it?
* on finding an open-paren, we are going to start building new room-chains off of the end of the room-chain when we got to the open-paren.  (Q: what about multiple inputs?)
  * an open-paren should mean we push all current-working-endpoints and push them onto the stack, so they become parent endpoints.
    * Note that this is the point where previously I had a problem losing the context between a single chain I was building (inside a parenthetical with pipe) vs. adding to all the parent chains; and I solved it by a recursion call, which at the outside of the call site retained all necessary context.
* each pipe tells us to stop adding to the current ends, and reset to the input parent input-ends and start again working from there.  Both the just-stopped chains' endpoints and the endpoints of the new chains we're starting to work on for the next pipe-section need to be put into the return set.
  * Note: if we recurse, we're working on a single subchain, but it's being added onto the entire parent set of endpoints, right?  When we un-recurse i.e. return, we're popping off a stack; we need clarity on what that means; it must mean the endpoints we're going to create _new_ subchains off of no longer include the ones we were working on before, but we're back to our parents?  This is a source of confusion and is probably a key to solving this well.
* 