from typing import List, Optional, Tuple

# DIRECTIONS
# "NOWHERE" is a valid move which means "this room" in a path; it's used to make a placeholder node (e.g. linked-list head).
NOWHERE = ''
NORTH = 'N'
EAST = 'E'
SOUTH = 'S'
WEST = 'W'

# Indexes of neighbors
NEIGHBOR_NORTH = 0
NEIGHBOR_EAST = 1
NEIGHBOR_SOUTH = 2
NEIGHBOR_WEST = 3

REVERSE_NEIGHBOR_INDEX_DIRECTIONS = [NORTH, EAST, SOUTH, WEST]


STEP_IN = '('  # This means "append all of the following subpaths to the current path(s), once we're done defining them"
START_NEW_SUBPATH = '|'  # This means "start a new subpath in the current group"
STEP_OUT = ')'  # This means "All subpaths in the current group are finalized and should be appended to the parent path(s)."
START = '^'
END = '$'


def get_neighbor_index(compass_direction: str) -> int:
    if compass_direction == NORTH:
        return 0
    elif compass_direction == EAST:
        return 1
    elif compass_direction == SOUTH:
        return 2
    elif compass_direction == WEST:
        return 3
    else:
        raise Exception(f'Programming error: invalid compass_direction "{compass_direction}".')


def anti(compass_direction: str) -> str:
    if compass_direction == NORTH:
        return SOUTH
    elif compass_direction == EAST:
        return WEST
    elif compass_direction == SOUTH:
        return NORTH
    elif compass_direction == WEST:
        return EAST
    else:
        raise Exception(f'Programming error: invalid compass_direction "{compass_direction}".')


class EndException(Exception):
    pass


def get_new_coordinate(compass_direction: str, cur_x: int, cur_y: int) -> Tuple[int, int]:
    if compass_direction == NORTH:
        return cur_x, cur_y - 1
    elif compass_direction == EAST:
        return cur_x + 1, cur_y
    elif compass_direction == SOUTH:
        return cur_x, cur_y + 1
    elif compass_direction == WEST:
        return cur_x - 1, cur_y
    else:
        raise Exception(f'Programming error: invalid compass_direction "{compass_direction}".')


class InputParser(object):
    def __init__(self, input_path_name: str):
        self.characters = self._load(input_path_name)
        self.cur_position = -1
        self.max_position = len(self.characters) - 1

    def get_next(self) -> str:
        if self.cur_position == self.max_position:
            raise EndException()
        self.cur_position += 1
        return self.characters[self.cur_position]

    def _load(self, input_path_name: str) -> str:
        line = open(input_path_name, 'r').read().strip()
        assert(line[0] == START)
        assert(line[-1] == END)
        for chara in line[1:-1]:
            assert chara in (STEP_IN, START_NEW_SUBPATH, STEP_OUT, NORTH, EAST, SOUTH, WEST)
        return line[1:-1]


class Move(object):
    """
    Represent one step in a room-definition path.
    Functions as a forward-linked-list node.
    """
    def __init__(self, direction):
        self.direction = direction
        self.child = None  # type: Optional[Move]


class Room(object):
    def __init__(self):
        self.x = -1
        self.y = -1
        self.center_mark = '.'
        self.neighbors = [None, None, None, None]  # Type: List[Optional[Room]]


def print_visual_map(rooms: List[Room]):
    # Make a grid, place the rooms in the grid, then render the rooms in left-to-right, top-to-bottom grid order
    #  so they can be printed.
    # Grid sizing: room centers are on even rows and columns; walls adjoining them are on odd rows and columns;
    #  number of rows and columns is always 2x number of rooms down/across, plus 1 more.
    min_x = min([room.x for room in rooms])
    max_x = max([room.x for room in rooms])
    min_y = min([room.y for room in rooms])
    max_y = max([room.y for room in rooms])
    width = max_x - min_x
    height = max_y - min_y

    rows = [['#'] * (width * 2 + 3) for y in range(height * 2 + 3)]
    for y in range(1, (max_y - min_y) * 2 + 2, 2):
        for x in range(1, (max_x - min_x) * 2 + 2, 2):
            rows[y][x] = ' '  # mark room as completely unmarked on the map.

    for room in rooms:
        visual_center_x = (room.x - min_x) * 2 + 1
        visual_center_y = (room.y - min_y) * 2 + 1

        for neighbor_direction, neighbor in enumerate(room.neighbors):
            if neighbor:
                door_x, door_y = get_new_coordinate(
                    REVERSE_NEIGHBOR_INDEX_DIRECTIONS[neighbor_direction], visual_center_x, visual_center_y)
                door_chara = '-' if door_x == visual_center_x else '|'
                rows[door_y][door_x] = door_chara
            rows[visual_center_y][visual_center_x] = room.center_mark

    for row in rows:
        print(''.join(row))

    # TODO: make a solid-walls-and-room-spots grid in rows and columns.
    # TODO: walk all the rooms and update their doors.
    # Q: is this easiest if we make a "grid" datatype?
    # TODO: print what we've got.


class PathGenerator(object):
    def __init__(self, input_parser: InputParser):
        self.input_parser = input_parser

    def __iter__(self):
        return self

    def __next__(self):



def run_main():
    subpath_group_stack = []  # type: List[List[Move]]
    cur_subpath_group = []  # type: List[Move]

    start = Move(NOWHERE)
    cur_subpath_group = [start]

    #parser = InputParser('real_input.txt')
    #parser = InputParser('sample_input_1.txt')
    #parser = InputParser('sample_input_2.txt')
    parser = InputParser('my_sample_1.txt')

    last_subpath_node = start
    while True:
        try:
            next_instruction = parser.get_next()
        except EndException:
            break
        if next_instruction in (NORTH, EAST, SOUTH, WEST):
            next_move = Move(next_instruction)
            # Q: do we actually need to append this move to ALL our current paths?
            last_subpath_node.child = next_move
            last_subpath_node = last_subpath_node.child
        elif next_instruction == STEP_IN:
            subpath_group_stack.append(cur_subpath_group)
            last_subpath_node = Move(NOWHERE)
            cur_subpath_group = [last_subpath_node]
        elif next_instruction == START_NEW_SUBPATH:
            last_subpath_node = Move(NOWHERE)
            cur_subpath_group.append(last_subpath_node)
        elif next_instruction == STEP_OUT:
            ### LEFT OFF HERE
            prior_ends_stack.pop()
            cur_room = prior_ends_stack[-1]
    print(f'Done; made {len(all_rooms)} rooms.')
    print_visual_map(all_rooms)


if __name__ == '__main__':
    run_main()
