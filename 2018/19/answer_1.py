from typing import List, NoReturn

class OpCode(object):
    ADD_IMM = 'addi'
    ADD_REG = 'addr'
    EQUAL_REG_REG = 'eqrr'
    GREATER_REG_REG = 'gtrr'
    MUL_REG = 'mulr'
    MUL_IMM = 'muli'
    SET_IMM = 'seti'
    SET_REGISTER = 'setr'


class Instruction(object):
    def __init__(self):
        self.op = '(NOT YET SET)'
        self.input_a = -1
        self.input_b = -1
        self.output_c = -1

    @staticmethod
    def load_from_str(str_form: str) -> 'Instruction':
        new_instance = Instruction()
        op, str_input_a, str_input_b, str_output_c = str_form.split()
        new_instance.op = op
        new_instance.input_a = int(str_input_a)
        new_instance.input_b = int(str_input_b)
        new_instance.output_c = int(str_output_c)
        return new_instance

    def __repr__(self) -> str:
        return f'Op {self.op}, in_A={self.input_a}, in_B={self.input_b}, out_C={self.output_c}'


class InvalidInstructionCounter(Exception):
    pass


class Machine(object):
    def __init__(self, instructions: List[Instruction]):
        self.registers = [0, 0, 0, 0, 0, 0]  # This starting register set is specific to Part 2 of the day-19 question
        self.shadow_instruction_counter_register = 2
        self.instruction_counter = 0
        self.instructions = instructions

    def run_instruction(self, instruction: Instruction) -> NoReturn:
        ix = instruction
        if ix.op == OpCode.ADD_IMM:
            self.registers[ix.output_c] = self.registers[ix.input_a] + ix.input_b
        elif ix.op == OpCode.ADD_REG:
            self.registers[ix.output_c] = self.registers[ix.input_a] + self.registers[ix.input_b]
        elif ix.op == OpCode.EQUAL_REG_REG:
            self.registers[ix.output_c] = 1 if self.registers[ix.input_a] == self.registers[ix.input_b] else 0
        elif ix.op == OpCode.GREATER_REG_REG:
            self.registers[ix.output_c] = 1 if self.registers[ix.input_a] > self.registers[ix.input_b] else 0
        elif ix.op == OpCode.MUL_IMM:
            self.registers[ix.output_c] = self.registers[ix.input_a] * ix.input_b
        elif ix.op == OpCode.MUL_REG:
            self.registers[ix.output_c] = self.registers[ix.input_a] * self.registers[ix.input_b]
        elif ix.op == OpCode.SET_IMM:
            self.registers[ix.output_c] = ix.input_a
        elif ix.op == OpCode.SET_REGISTER:
            self.registers[ix.output_c] = self.registers[ix.input_a]
        else:
            raise Exception(f'No case for opcode "{ix.op}".')

    def step(self):
        # Day 19 thing: copy IC down to shadow-register
        self.registers[self.shadow_instruction_counter_register] = self.instruction_counter

        if 0 > self.instruction_counter or self.instruction_counter > len(self.instructions) - 1:
            print(f'Invalid instruction counter: {self.instruction_counter}.')
            raise InvalidInstructionCounter()

        self.run_instruction(self.instructions[self.instruction_counter])

        # Day 19 thing: copy shadow-register back up to IC after running opcode.
        self.instruction_counter = self.registers[self.shadow_instruction_counter_register]
        # Increment IC _after_ running opcode AND, of course, after copying the shadow-register back up
        self.instruction_counter += 1


def run_main():
    instructions = [Instruction.load_from_str(line.strip())
                    for line in open('real_input.txt', 'r').readlines()
                    if line.strip() and not line.startswith('#')]
    #print(instructions)
    machine = Machine(instructions)
    num_steps = 0
    try:
        while True:
            machine.step()

            num_steps += 1
    except InvalidInstructionCounter:
        print('Machine execution concluded.')
        print(f'Machine registers: {machine.registers}')
        print(f'Number of steps successfully concluded: {num_steps}')
        print(f'Final reg0 value: {machine.registers[0]}.')
        # Correct answer for reg 0 final value when registers all start as 0: 1464
        # (This took 7,593,316 steps.)


if __name__ == '__main__':
    run_main()
