from typing import List

def get_unique_factors(factand: int) -> List[int]:
    factors = []  # type: List[int]

    for i in range(1, factand + 1):
        if factand % i == 0:
            factors.append(i)
    return factors


def run_main():
    factand = 974
    factors = get_unique_factors(factand)
    print(f'The {len(factors)} unique factors of {factand} sum to {sum(factors)}.')

    factand = 10551374
    factors = get_unique_factors(factand)
    print(f'The {len(factors)} unique factors of {factand} sum to {sum(factors)}.')


if __name__ == '__main__':
    run_main()