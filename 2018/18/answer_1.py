from typing import Dict, List, NoReturn, Tuple

TREE = '|'
YARD = '#'
OPEN = '.'


def num_in(test_chara: str, list_of_charas: List[str]) -> int:
    return len([c for c in list_of_charas if c == test_chara])



class Board(object):
    def __init__(self):
        self.width = -1
        self.spots = []
        self.neighbor_indexes = {}  # type: Dict[int, List[int]]

    def load_from_file(self, in_file_path: str) -> NoReturn:
        for line in [line.strip() for line in open(in_file_path, 'r').readlines()]:
            if self.width > 0:
                if self.width != len(line):
                    raise Exception(f'Uneven line widths detected - {self.width} vs. {len(line)}.')
            else:
                self.width = len(line)
            for chara in line:
                if chara not in (TREE, YARD, OPEN):
                    raise Exception(f'Unknown character in input "{chara}".')
                self.spots.append(chara)
        self._build_neighbors()

    def _build_neighbors(self) -> NoReturn:
        for i in range(len(self.spots)):
            self.neighbor_indexes[i] = self._get_neighbor_indexes(i)

    def _get_neighbor_indexes(self, index: int) -> List[int]:
        row = index // self.width
        col = index % self.width
        max_row = len(self.spots) // self.width - 1
        max_col = self.width - 1
        neighbor_indexes = []
        if row > 0:
            if col > 0:
                neighbor_indexes.append(index - self.width - 1)
            neighbor_indexes.append(index - self.width)
            if col < max_col:
                neighbor_indexes.append(index - self.width + 1)
        if col > 0:
            neighbor_indexes.append(index - 1)
        if col < max_col:
            neighbor_indexes.append(index + 1)
        if row < max_row:
            if col > 0:
                neighbor_indexes.append(index + self.width - 1)
            neighbor_indexes.append(index + self.width)
            if col < max_col:
                neighbor_indexes.append(index + self.width + 1)
        return neighbor_indexes

    def _get_neighbors(self, index: int) -> List[str]:
        return [self.spots[i] for i in self.neighbor_indexes[index]]

    def _compute_next_state(self, index: int) -> str:
        cur = self.spots[index]
        neighbors = self._get_neighbors(index)
        if cur == OPEN:
            if num_in(TREE, neighbors) >= 3:
                return TREE
            else:
                return OPEN
        if cur == TREE:
            if num_in(YARD, neighbors) >= 3:
                return YARD
            else:
                return TREE
        if num_in(YARD, neighbors) >= 1 and num_in(TREE, neighbors) >= 1:
            return YARD
        else:
            return OPEN

    def step(self) -> NoReturn:
        self.spots = [self._compute_next_state(i) for i in range(len(self.spots))]

    def __str__(self) -> str:
        out_charas = []
        counter = 0
        for chara in self.spots:
            if counter == self.width:
                out_charas.append('\n')
                counter = 0
            out_charas.append(chara)
            counter += 1
        return ''.join(out_charas)


def get_value(board: Board) -> Tuple[int, int, int]:
    num_tree = num_in(TREE, board.spots)
    num_yard = num_in(YARD, board.spots)
    return num_tree, num_yard, num_tree * num_yard


def get_expected_value(iteration_num: int) -> int:
    values = (214560, 210960, 207640, 201123, 196768, 192552, 191416, 188244, 188670, 187530, 189663, 189904, 192625,
              193101, 193536, 194688, 195797, 198068, 200349, 202806, 204600, 208292, 211050, 214524, 214977, 214524,
              215130, 214557)
    return values[iteration_num % 28]


def run_main_2() -> NoReturn:
    print(get_expected_value(1000000000 - 1))

def run_main() -> NoReturn:
    board = Board()
    #board.load_from_file('sample_input.txt')
    board.load_from_file('input_1.txt')
    print(board)
    print()
    #for i in range(10):
    known_board_states = dict()  # type: Dict[str, int]
    for i in range(1000000000):
        board.step()
#        print(board)
#        print()

        if i > 800:  # we have generally hit about 10 repeats of a given value as of 800 reps.
            #board_state = str(board)
            #print(f'Rep: {i} Value: {get_value(board)[2]}')
            value = get_value(board)[2]
            assert(value == get_expected_value(i))
            print(':)')
        if i > 1000:
            exit()

    #print(board)
    num_tree, num_yard, total_value = get_value(board)
    print(f'Value: {num_tree} trees * {num_yard} yards = {num_tree * num_yard}')
    # answer of 188244 for what I think is final state is wrong - too low.
    # 2nd guess: 202806


if __name__ == '__main__':
    #run_main()
    run_main_2()