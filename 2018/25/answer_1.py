from typing import List, NoReturn, Tuple


def distance(alpha: Tuple[int, int, int, int], bravo: Tuple[int, int, int, int]) -> int:
    return abs(alpha[0] - bravo[0]) + abs(alpha[1] - bravo[1]) + abs(alpha[2] - bravo[2]) + abs(alpha[3] - bravo[3])


class Constellation(object):
    def __init__(self, initial_node: Tuple[int, int, int, int]):
        self.nodes = set()
        self.nodes.add(initial_node)

    def add_member_if_local(self, test_node: Tuple[int, int, int, int]) -> bool:
        for existing_node in self.nodes:
            if distance(existing_node, test_node) <= 3:
                self.nodes.add(test_node)
                return True
        return False

    def join_constellation(self, other: 'Constellation') -> bool:
        """
        Make both self and other constellation contain the same superset of both their starting nodes.
        Returns bool saying whether anything changed (i.e. whether the two constellations were not already equal).
        """
        if self == other:
            return False

        self.nodes.update(other.nodes)
        other.nodes.update(self.nodes)
        return True

    def __eq__(self, other: 'Constellation') -> bool:
        return self.nodes == other.nodes

    def __hash__(self) -> int:
        return hash(tuple(sorted(self.nodes)))


def run_main(input_filename: str) -> NoReturn:
    coord_lines = [line.strip() for line in open(input_filename, 'r').readlines() if line.strip()]
    nodes = []  # Type: List[int, int, int, int]

    for line in coord_lines:
        pieces = line.split(',')
        nodes.append((int(pieces[0]), int(pieces[1]), int(pieces[2]), int(pieces[3])))

    constellations = [Constellation(nodes[0])]

    # First, naively fit into constellations.  This can create excess constellations due to ordering.
    for node in nodes[1:]:
        fits_existing_constellation = False
        for constellation in constellations:
            if constellation.add_member_if_local(node):
                fits_existing_constellation = True
                break
        if not fits_existing_constellation:
            constellations.append(Constellation(node))

    print('Done computing node-node distances and creating initial (naive) constellations.')

    constellation_joins = []  # type: List[Tuple[Constellation, Constellation]]
    # Second, re-check every node in every constellation against every node in all _other_ constellations to see
    #  if they would have been close enough to be in the same constellation, and keep a list of joins.
    for constellation in constellations:
        for node in constellation.nodes:
            for other_constellation in constellations:
                if constellation == other_constellation:
                    continue
                for other_node in other_constellation.nodes:
                    if distance(node, other_node) <= 3:
                        constellation_joins.append((constellation, other_constellation))
                        break


    print('Done rechecking node-node distances and creating constellation join declarations.')

    # Join all constellations; this is idempotent and short-circuited, so we don't need to worry about repeats.
    #  Run the list repeatedly until nothing changes, to join up any chains.
    change_seen = True
    while change_seen:
        change_seen = False
        for c1, c2 in constellation_joins:
            if c1.join_constellation(c2):
                change_seen = True

    print('Done joining constellations.')

    # get just unique constellations now
    constellations = list(set(constellations))

    print(f'Total number of constellations from file {input_filename}: {len(constellations)}.')


if __name__ == '__main__':
    #run_main('sample_input_3.txt')
    for input_filename in ('sample_input_2.txt', 'sample_input_3.txt', 'sample_input_4.txt', 'sample_input_8.txt'):
        run_main(input_filename)
    run_main('real_input.txt')
    # output is 434 unique constellations.  So that is our first guess.
    #  This number was declared wrong - too high.
    # I removed an early exit from the constellation-joining-checking which would prevent a node from being able to
    #  join 3 or more constellations together, and reran; the result is now 425, so this is my second submission.
    #  (Sample input results still yield the correct values.)
    #  This was the correct answer.

# Note 2020-12-20: Part 2 won't unlock until I have 49 stars, it says.