class Foo(object):
    def __init__(self, initial):
        self.initial = initial

    def __eq__(self, other):
        return self.initial == other.initial

    def __hash__(self):
        return hash(self.initial)
          
         

s = set()
s.add(Foo(5))
s.add(Foo(5))
s.add(Foo(6))
print(s)