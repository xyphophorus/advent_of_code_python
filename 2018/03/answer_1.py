# Note: Rectangles are 4-tuples of left, bottom, right, top
# Note: done without type-hints for readability/teachability to noobs
# Note: we are using a classic graph-paper x progresses left-right and y progresses bottom-to-top
#        coordinate system, again for noob comfort, even though some/many graphics layouts invert the Y.


def rectangle_from_input_line(input_line):
    """
    Given an input file line, which gives the coordinates of a corner of the rectangle and the width and height,
     return the 4-tuple of the rectangle's overall coordinates - left, bottom, right, and top.  (This is also
     equivalent to, although grouped differently from, returning a sequence of two points: the lower-left point
     and the upper-right point.)
    """
    # Sample input line:  #1 @ 829,837: 11x22
    # Note that the size of the rectangle - width and height - includes the row and column of the described corner.
    #  This means that the other corner is at (x + width - 1, y + height - 1).
    ident_part, shape_part = input_line.split('@')
    point_part, size_part = shape_part.split(':')
    point_part = point_part.strip()
    size_part = size_part.strip()
    x_string, y_string = point_part.split(',')
    w_string, h_string = size_part.split('x')

    # we validate the results of our parsing by seeing that we have only numbers left.
    corner_x = int(x_string.strip())
    corner_y = int(y_string.strip())
    width = int(w_string.strip())
    height = int(h_string.strip())

    assert(width > 0)
    assert(height > 0)

    return corner_x, corner_y, corner_x + width - 1, corner_y + height - 1


def get_rectangles(input_file_path):
    out_rectangles = []
    for line in open(input_file_path, 'r').readlines():
        if not line.strip():
            continue
        out_rectangles.append(rectangle_from_input_line(line))
    return out_rectangles


def get_extents(rectangles):
    """ Get the superset rectangle which includes all input rectangles. """
    min_x, min_y, max_x, max_y = rectangles[0]

    for rectangle in rectangles:
        # we separate the unpack from the loop iteration/definition for clarity here.
        left, bottom, right, top = rectangle
        min_x = min(min_x, left)
        min_y = min(min_y, bottom)
        max_x = max(max_x, right)
        max_y = max(max_y, top)

    return min_x, min_y, max_x, max_y


def get_grid_index(row_width, x_offset, y_offset, x_location, y_location):
    zero_based_row_number = y_location - y_offset
    zero_based_column_number = x_location - x_offset
    return zero_based_row_number * row_width + zero_based_column_number


def increment_grid_spot(storage_array, row_width, x_offset, y_offset, x_location, y_location):
    """
    Find the correct spot in the storage (byte) array and increment it.
    Note that since we're using bytes, we cannot safely store a value more than 255, so if we would do so,
     we will instead just print a warning.  This means the value would be wrong (if we needed it to be precisely
     correct for values above 255) but also the program can keep running and by watching the console output we
     will be notified if this happens and can decide if we need to take further action.
    """
    index = get_grid_index(row_width, x_offset, y_offset, x_location, y_location)

    existing_value = storage_array[index]

    if 255 == existing_value:
        print(f'Warning: grid location ({x_location}, {y_location}) has value 255 and cannot be incremented.')
    else:
        storage_array[index] = existing_value + 1


# This is needed only for answer part 2.
def read_grid_spot(storage_array, row_width, x_offset, y_offset, x_location, y_location):
    index = get_grid_index(row_width, x_offset, y_offset, x_location, y_location)
    return storage_array[index]


def get_all_spots_in_rectangle(rectangle_left, rectangle_bottom, rectangle_right, rectangle_top):
    """
    Returns a list of 2-tuples, where each 2-tuple is (x, y) of one point.
    All input rectangle coordinates are considered inclusive.
    The points returned will be unique (i.e. no point will be returned twice).
    """
    spots = []
    for x in range(rectangle_left, rectangle_right + 1):
        for y in range(rectangle_bottom, rectangle_top + 1):
            spots.append((x, y))
    return spots


if __name__ == '__main__':
    input_file_path = 'real_input.txt'
    all_rectangles = get_rectangles(input_file_path)

    grid_left, grid_bottom, grid_right, grid_top = get_extents(all_rectangles)

    # Our array(s) will, of course, start at index 0.  But the grid extents we will represent in the array(s) may not.
    #  The difference is simply the left and bottom edge coordinates of our extents; these are offsets which will
    #  allow us to convert between 2-D grid location and internal array storage location (index).
    x_offset = grid_left
    y_offset = grid_bottom

    # We add one to get width and height because the values are inclusive.
    #  A row reaching from x=2 to x=4 contains the x spot values 2, 3, 4.
    #  A column will work the same way.
    row_width = grid_right - grid_left + 1
    col_height = grid_top - grid_bottom + 1

    print(f'Grid width: {row_width}; height: {col_height}')
    print(f'Grid storage size: {row_width * col_height}.')
    grid_storage = bytearray(row_width * col_height)

    # Increment all the grid spots from input rectangles.
    for input_rectangle in all_rectangles:
        # TODO: use unpack operator in this next line after explaining it.
        spots = get_all_spots_in_rectangle(input_rectangle[0], input_rectangle[1], input_rectangle[2], input_rectangle[3])
        for spot in spots:
            increment_grid_spot(grid_storage, row_width, x_offset, y_offset, spot[0], spot[1])

    # For the answer to part 1 of the question, count how many spots were indicated by two or more rectangles.
    over_subscribed_count = 0
    for storage_location in grid_storage:
        if storage_location > 1:
            over_subscribed_count += 1
    print(f'{over_subscribed_count} grid spots were claimed by more than one rectangle.')

    # For the answer to part 2 of the question, they want to know the ID of the single rectangle which does not
    #  overlap with any other rectangles.  To find this, we can just look at every rectangle, and if it has any
    #  spots which are claimed by more than 1 rectangle in our grid_storage above, reject it.  We should be left
    #  with only a single rectangle.
    # We discarded the ID of each rectangle, but a quick check of real_input.txt in notepad++ shows that the ID
    #  of each rectangle matches its 1-based row number, and we retained the original order of the rectangles, so
    #  we have this information.
    rectangle_id = 1
    for input_rectangle in all_rectangles:
        rectangle_has_oversubscribed_spot = False
        spots = get_all_spots_in_rectangle(input_rectangle[0], input_rectangle[1], input_rectangle[2],
                                           input_rectangle[3])
        for spot in spots:
            if read_grid_spot(grid_storage, row_width, x_offset, y_offset, spot[0], spot[1]) > 1:
                rectangle_has_oversubscribed_spot = True

        if not rectangle_has_oversubscribed_spot:
            print(f'Rectangle ID {rectangle_id} does not have any spots that overlap with any other rectangle.')

        rectangle_id += 1
