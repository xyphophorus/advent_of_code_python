# purpose: find the two rows in the input which differ by exactly 1 character, and return their common letters.
# As the input contains 250 rows, one way is to compare all 250 rows to all 249 other rows,
#  resulting in 62,250 comparisons.  As comparing two strings to see if they're different
#  by exactly one character is a relatively quick operation, this seems like a reasonable
#  way to do it.

from typing import List


def are_exactly_one_char_different(alpha: str, bravo: str) -> bool:
    # Assumption: input strings are the same length.  This happens to be true for the inputs.
    difference_seen = False
    for i in range(len(alpha)):
        if alpha[i] != bravo[i]:
            # The second difference is disqualifying.
            if difference_seen:
                return False
            else:
                difference_seen = True
    # At this point we've either seen 0 differences or exactly 1 difference, since
    #  we would have returned earlier if we'd seen a second difference.
    return difference_seen


def get_common_letters(alpha: str, bravo: str) -> str:
    # Assumption: input strings are the same length.  This happens to be true for the inputs.
    out_charas = []  # type: List[str]
    for i in range(len(alpha)):
        if alpha[i] == bravo[i]:
            out_charas.append(alpha[i])
    return ''.join(out_charas)


def run_main():
    lines = [line.strip() for line in open('real_input.txt', 'r').readlines() if line.strip()]

    for i, alpha_line in enumerate(lines):
        for j, bravo_line in enumerate(lines):
            if i == j:
                continue
            if are_exactly_one_char_different(alpha_line, bravo_line):
                print(get_common_letters(alpha_line, bravo_line))
                exit()


if __name__ == '__main__':
    run_main()

# Postscript: running however many comparisons were actually necessary ended up taking less than 1 second of runtime.