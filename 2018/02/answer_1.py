# purpose: find all rows in the input which have one or more characters which appears exactly 2 or exactly 3 times.
#  multiply the counts of those two conditions together to get our output answer.


class CountResult(object):
    def __init__(self, has_double: bool, has_triple: bool):
        self.has_double = has_double
        self.has_triple = has_triple


def count_doubles_and_triples(in_string: str) -> CountResult:
    char_counts = dict()

    for chara in in_string:
        if chara not in char_counts:
            char_counts[chara] = 1
        else:
            char_counts[chara] += 1

    has_double = any([value for value in char_counts.values() if value == 2])
    has_triple = any([value for value in char_counts.values() if value == 3])
    return CountResult(has_double, has_triple)


def run_main():
    lines = [line.strip() for line in open('real_input.txt', 'r').readlines() if line.strip()]
    num_doubles = 0
    num_triples = 0
    for line in lines:
        count_result = count_doubles_and_triples(line)
        if count_result.has_double:
            num_doubles += 1
        if count_result.has_triple:
            num_triples += 1
    print(f'{num_doubles} lines have doubles, and {num_triples} lines have triples.')
    print(f'The product of those numbers is {num_doubles * num_triples}.')


if __name__ == '__main__':
    run_main()

