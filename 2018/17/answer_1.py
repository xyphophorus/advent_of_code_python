#input_file = 'input_sample_1.txt'
input_file = 'input_1.txt'

CLAY = '#'
SAND = '.'
RUNNING = '|'
STANDING = '~'
SPRING = '+'


def load_lines(in_file_path):
    return [line.strip() for line in open(in_file_path, 'r').readlines() if line.strip()]


def assert_equals(obj1, obj2):
    if obj1 != obj2:
        fail_msg = f'Mismatch: "{obj1}" != "{obj2}"'
        raise Exception(fail_msg)


class WaterChange(object):
    def __init__(self):
        self.num_changed = 0
        self.fell_points = []
        self.oob_fall_points = []
        self.spread_points = []
        self.drop_points = []


class GroundGraph(object):
    def __init__(self, min_x, max_x, min_y, max_y):
        self.min_x = min_x
        self.max_x = max_x
        self.min_y = min_y
        self.max_y = max_y
        self.rows = []
        self._init_rows()
        self.spring = (500, 0)
        self.set_spring(*self.spring)

    def _init_rows(self):
        for x in range(self.min_y, self.max_y + 1):
            row = ['.'] * (self.max_x - self.min_x + 1)
            self.rows.append(row)

    def set_clay(self, x, y):
        self.rows[y - self.min_y][x - self.min_x] = CLAY

    def set_spring(self, x, y):
        self.rows[y - self.min_y][x - self.min_x] = SPRING

    def set_running(self, x, y):
        self.rows[y - self.min_y][x - self.min_x] = RUNNING

    def set_standing(self, x, y):
        self.rows[y - self.min_y][x - self.min_x] = STANDING

    def get(self, x, y):
        return self.rows[y - self.min_y][x - self.min_x]

    def get_all_wet_blocks(self):
        wet_points = []
        for y_index, row in enumerate(self.rows):
            for x_index, chara in enumerate(row):
                if chara in (STANDING, RUNNING):
                    wet_points.append((x_index + self.min_x, y_index + self.min_y))
        return wet_points

    def get_all_standing_blocks(self):
        standing_points = []
        for y_index, row in enumerate(self.rows):
            for x_index, chara in enumerate(row):
                if chara in (STANDING):
                    standing_points.append((x_index + self.min_x, y_index + self.min_y))
        return standing_points

    def get_water_spread_points(self, start_x, start_y):
        row = self.rows[start_y - self.min_y]
        start_index = start_x - self.min_x
        left_index = start_index
        right_index = start_index
        out_points = []
        while left_index > 0 and row[left_index - 1] != CLAY \
                and not self.can_water_fall_here(left_index + self.min_x, start_y):
            left_index -= 1
            out_points.append((left_index + self.min_x, start_y))
        while right_index < len(row) - 1 and row[right_index + 1] != CLAY \
                and not self.can_water_fall_here(right_index + self.min_x, start_y):
            right_index += 1
            out_points.append((right_index + self.min_x, start_y))
        # sort left-to-right
        return sorted(out_points)

    def is_next_drop_point_oob(self, test_x, test_y):
        """ Tell whether water falling from the specified point would be off the bottom of the map. """
        return (test_y - self.min_y) == len(self.rows) - 2

    def can_water_fall_here(self, test_x, test_y):
        """ Tell whether water can legimitately fall down from the specified point. """
        return not self.is_next_drop_point_oob(test_x, test_y) \
               and self.rows[test_y - self.min_y + 1][test_x - self.min_x] in (SAND, RUNNING)

    def get_water_fall_points(self, start_x, start_y):
        y_index = start_y - self.min_y
        x_index = start_x - self.min_x
        out_points = []
        while y_index < len(self.rows) - 2 and self.rows[y_index + 1][x_index] not in (CLAY, STANDING):
            y_index += 1
            out_points.append((start_x, y_index + self.min_y))
        return out_points

    def fill_water(self, start_x, start_y):
        result = WaterChange()
        fall_points = self.get_water_fall_points(start_x, start_y)
        if not fall_points:
            return result
        for fall_point in fall_points:
            result.fell_points.append(fall_point)
            if self.get(*fall_point) == SAND:
                self.set_running(*fall_point)  # Minor bug: if this ends up being standing water, we re-set it later, which could cause problems
                result.num_changed += 1
        if self.is_next_drop_point_oob(*fall_points[-1]):
            result.oob_fall_points.append((fall_points[-1][0], fall_points[-1][1] + 1))
            return result
        spread_points = self.get_water_spread_points(*fall_points[-1])
        result.spread_points = spread_points
        drop_points = []
        if spread_points and self.can_water_fall_here(*spread_points[0]):
            drop_points.append(spread_points[0])
        if len(spread_points) > 1 and self.can_water_fall_here(*spread_points[-1]):
            drop_points.append(spread_points[-1])
        result.drop_points = drop_points
        if not drop_points:  # no drop points means we found a new standing level.
            for spread_point in spread_points:
                if self.get(*spread_point) == SAND:  # avoid double-counting the fall point to this level.
                    result.num_changed += 1
                self.set_standing(*spread_point)
            self.set_standing(*fall_points[-1])
        else:  # we have one or more drop points, so our spread water is all running, not standing.
            for spread_point in spread_points:
                if self.get(*spread_point) == SAND:  # avoid double-counting the fall point to this level.
                    result.num_changed += 1
                self.set_running(*spread_point)
#            # recurse on the left drop from our spread
#            result.num_changed += self.fill_water(*spread_points[0])
#            # if there were two drop sides to our spread, recurse on the right side as well
#            if len(spread_points) > 1:
#                result.num_changed += self.fill_water(*spread_points[-1])
        return result

    def __str__(self):
        return self.get_top_lines(-1)

    def get_lines(self, start_index, end_index):
        lines = [''.join(row) for row in self.rows][start_index: end_index]
        return '\n'.join(lines)

    def get_top_lines(self, num_top_lines):
        lines = [''.join(row) for row in self.rows]
        if num_top_lines > 0:
            lines = lines[:num_top_lines]
        return '\n'.join(lines)

    def get_bottom_lines(self, num_bottom_lines):
        lines = [''.join(row) for row in self.rows]
        if num_bottom_lines > 0:
            lines = lines[-1 * num_bottom_lines:]
        return '\n'.join(lines)


def get_extents(points):
    min_x = min([p[0] for p in points])
    max_x = max([p[0] for p in points])
    #min_y = min([p[1] for p in points])
    max_y = max([p[1] for p in points])
    return min_x - 1, max_x + 1, 0, max_y + 1


def test_get_extents():
    cut = get_extents
    assert_equals(cut([(1, 1), (3, 3)]), (0, 4, 0, 4))
    assert_equals(cut([(50, 1), (500, 13)]), (49, 501, 0, 14))


# TODO: unit test this
def scan_str_to_range(coord_spec):
    if '..' not in coord_spec:
        return [int(coord_spec)]
    left, right = coord_spec.split('..')
    return list(range(int(left), int(right) + 1))


def test_scan_str_to_range():
    cut = scan_str_to_range
    assert_equals(cut('5'), [5])
    assert_equals(cut('5..7'), [5, 6, 7])


def get_points(scan_line):
    left, right = scan_line.split(', ')
    x = left[2:] if left[0] == 'x' else right[2:]
    y = left[2:] if left[0] == 'y' else right[2:]
    x_coords = scan_str_to_range(x)
    y_coords = scan_str_to_range(y)
    points = []
    for x in x_coords:
        for y in y_coords:
            points.append((x, y))
    return points


def test_get_points():
    cut = get_points
    assert_equals(cut('x=495, y=2..4'), [(495, 2), (495, 3), (495, 4)])
    assert_equals(cut('y=2..4, x=495'), [(495, 2), (495, 3), (495, 4)])
    assert_equals(cut('y=3, x=4'), [(4, 3)])
    assert_equals(cut('x=3, y=4'), [(3, 4)])


def load_ground():
    points = []
    scan_lines = load_lines(input_file)
    for scan_line in scan_lines:
        points.extend(get_points(scan_line))
    min_x, max_x, min_y, max_y = get_extents(points)
    ground = GroundGraph(min_x, max_x, min_y, max_y)
    for point in points:
        ground.set_clay(point[0], point[1])
    return ground


def run_tests():
    test_scan_str_to_range()
    test_get_points()
    test_get_extents()


def run_main():
    DO_DEBUG_UI = False
    ground = load_ground()
    water_change = WaterChange()
    water_change.num_changed = -1
    points_to_fill_from = set()
    points_to_fill_from.add(ground.spring)
    #while water_change.num_changed != 0 and points_to_fill_from:
    while points_to_fill_from or not water_change.oob_fall_points:
        if points_to_fill_from:
            point = points_to_fill_from.pop()
        else:
            point = ground.spring
        water_change = ground.fill_water(*point)
        for point in water_change.drop_points:
            points_to_fill_from.add(point)
        if DO_DEBUG_UI and water_change.num_changed > 0:
            start, end = eval(open('view_lines.txt', 'r').read())
            print(ground.get_lines(int(start), int(end)))
            print(f'{water_change.num_changed} changed.')
            input('Press <Enter> to continue...')
        elif water_change.num_changed > 0:
            pass
            #print(f'{water_change.num_changed} changed.')
    num_total_wet = len(ground.get_all_wet_blocks())  # NOTE: THIS IS OFF BECAUSE it counts the couple of top rows higher than any clay.
    num_total_standing = len(ground.get_all_standing_blocks())
    #print(ground)
    print(f'Total wet blocks: {num_total_wet}')
    print(f'Total standing blocks: {num_total_standing}')


if __name__ == '__main__':
    run_tests()
    run_main()
    # First part: Total number of wet blocks:
    # WRONG ANSWER: 31790 (too high)
    # WRONG ANSWER: 31608 (too low) (cutting off the left-most and right-most columns, one of which had a waterfall in it, which I thought was invalid... but I'm wrong; all x coords are valid.)
    # guess: 31788 - this is 31790 minus the first two rows that don't have any clay in them.  IT'S CORRECT!
    # Second part: Total number of standing blocks:
    # First guess is correct - 25800

# Water-finding:
#  1. descend from source until the next square would be either clay or standing water.
#  2. from that bottom-most square found, scan left (and right) until the first spot found with sand under it; abort that
#      direction of scanning if the next square to the left/right is clay.  (Standing water should not be an option.)
#    2A. All these square crossed will become standing water IF we are blocked in both directions by clay.  They become
#        falling water if we find sand in at least one direction.
#  3. the iteration stops when we are blocked in both directions by clay, and our most recent left-right scan pair become standing water.
#  4. if we are falling through sand and find the end of the map, that was our final iteration.