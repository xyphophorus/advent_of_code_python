from typing import List, NoReturn

# REAL input values:
CAVE_DEPTH = 11820
COORD_TARGET = (7, 782)
COORD_MOUTH = (0, 0)

#TEST input values:
#CAVE_DEPTH = 510
#COORD_TARGET = (10, 10)
#COORD_MOUTH = (0, 0)

NOT_COMPUTED = -1
TYPE_ROCKY = 0
TYPE_WET = 1
TYPE_NARROW = 2

DISPLAY_TYPES = ('.', '=', '|')


def erosion_for_geo(geo_index: int) -> int:
    """ Given a geological index, return the erosion level. """
    return (geo_index + CAVE_DEPTH) % 20183


class EarthGraph(object):
    def __init__(self, width: int, height: int):
        self.width = width
        self.height = height
        self.geo_rows = []  # type: List[List[int]]
        self.erosion_rows = []  # type: List[List[int]]
        self.type_rows = []  # type: List[List[int]]
        for i in range(height):
            self.geo_rows.append([NOT_COMPUTED] * width)
            self.erosion_rows.append([NOT_COMPUTED] * width)
            self.type_rows.append([NOT_COMPUTED] * width)
        self._compute_all_values()

    def set_geo(self, x: int, y: int, value: int) -> NoReturn:
        self.geo_rows[y][x] = value
        self.set_erosion(x, y, erosion_for_geo(value))

    def set_erosion(self, x: int, y: int, value: int) -> NoReturn:
        self.erosion_rows[y][x] = value

    def get_erosion(self, x: int, y: int) -> int:
        return self.erosion_rows[y][x]

    def get_type(self, x: int, y: int) -> int:
        return self.type_rows[y][x]

    def set_type(self, x: int, y: int, value: int) -> NoReturn:
        self.type_rows[y][x] = value

    def get_risk_level(self, max_x: int, max_y: int) -> int:
        level = 0
        for y in range(max_y + 1):
            for x in range(max_x + 1):
                level += self.get_type(x, y)
        return level

    def _compute_all_values(self) -> NoReturn:
        # set geo for known values
        self.set_geo(*COORD_MOUTH, 0)
        self.set_geo(*COORD_TARGET, 0)

        # set geo for the left and top borders, which are directly computable.
        for x in range(self.width):
            if x != 0:
                self.set_geo(x, 0, x * 16807)
        for y in range(self.height):
            if y != 0:
                self.set_geo(0, y, y * 48271)

        # set geo for all those which depend on others.  Working left-to-right, top-to-bottom should be computable
        #  in a single pass.  (Also, top-to-bottom, left-to-right.)
        for y in range(self.height):
            for x in range(self.width):
                if (x, y) in (COORD_MOUTH, COORD_TARGET) or x == 0 or y == 0:
                    continue
                self.set_geo(x, y, self.get_erosion(x - 1, y) * self.get_erosion(x, y - 1))

        # set types (for visualization)
        for y in range(self.height):
            for x in range(self.width):
                self.set_type(x, y, self.get_erosion(x, y) % 3)


def print_graph(graph: EarthGraph) -> NoReturn:
    for y, row in enumerate(graph.type_rows):
        charas = [DISPLAY_TYPES[terrain_type] for terrain_type in row]
        if y == 0:
            charas[0] = 'M'
        elif y == COORD_TARGET[1]:
            charas[COORD_TARGET[0]] = 'T'
        print(''.join(charas))


def run_main():
    graph = EarthGraph(COORD_TARGET[0] + 1, COORD_TARGET[1] + 1)
    print_graph(graph)

    print(f'Risk level to target: {graph.get_risk_level(*COORD_TARGET)}')


if __name__ == '__main__':
    run_main()

# First guess: 6318
#  Correct!
