from typing import List, NoReturn, Optional, Tuple

# REAL input values:
# CAVE_DEPTH = 11820
# COORD_TARGET = (7, 782)
# COORD_MOUTH = (0, 0)

#TEST input values:
CAVE_DEPTH = 510
COORD_TARGET = (10, 10)
COORD_MOUTH = (0, 0)

NOT_COMPUTED = -1
TYPE_ROCKY = 0
TYPE_WET = 1
TYPE_NARROW = 2

# The values used here don't matter, although we do want them to be different from anything else we've used, and
#  for safety they shouldn't typically be valid as indexes for something.  Also, they're powers of 2 so they can be
#  combined as bits.
TOOL_CLIMB = 4
TOOL_NEITHER = 8
TOOL_TORCH = 16
ALL_TOOLS = (TOOL_CLIMB, TOOL_NEITHER, TOOL_TORCH)

DISPLAY_TYPES = ('.', '=', '|')


def erosion_for_geo(geo_index: int) -> int:
    """ Given a geological index, return the erosion level. """
    return (geo_index + CAVE_DEPTH) % 20183


class EarthGraph(object):
    def __init__(self, width: int, height: int):
        self.width = width
        self.height = height
        self.geo_rows = []  # type: List[List[int]]
        self.erosion_rows = []  # type: List[List[int]]
        self.type_rows = []  # type: List[List[int]]
        self.allowed_tool_rows = []  # type: List[List[int]]

        for i in range(height):
            self.geo_rows.append([NOT_COMPUTED] * width)
            self.erosion_rows.append([NOT_COMPUTED] * width)
            self.type_rows.append([NOT_COMPUTED] * width)
            self.allowed_tool_rows.append([0] * width)  # initial state must be 0 because this is a bitfield.
        self._compute_all_values()

    def set_geo(self, x: int, y: int, value: int) -> NoReturn:
        self.geo_rows[y][x] = value
        self.set_erosion(x, y, erosion_for_geo(value))

    def set_erosion(self, x: int, y: int, value: int) -> NoReturn:
        self.erosion_rows[y][x] = value

    def get_erosion(self, x: int, y: int) -> int:
        return self.erosion_rows[y][x]

    def get_type(self, x: int, y: int) -> int:
        return self.type_rows[y][x]

    def set_type(self, x: int, y: int, value: int) -> NoReturn:
        self.type_rows[y][x] = value

    def set_allowed_tools(self, x: int, y: int, combined_values: int) -> NoReturn:
        self.allowed_tool_rows[y][x] = combined_values

    def get_risk_level(self, max_x: int, max_y: int) -> int:
        level = 0
        for y in range(max_y + 1):
            for x in range(max_x + 1):
                level += self.get_type(x, y)
        return level

    def _compute_geo_values(self) -> NoReturn:
        # set geo for known values
        self.set_geo(*COORD_MOUTH, 0)
        self.set_geo(*COORD_TARGET, 0)

        # set geo for the left and top borders, which are directly computable.
        for x in range(self.width):
            if x != 0:
                self.set_geo(x, 0, x * 16807)
        for y in range(self.height):
            if y != 0:
                self.set_geo(0, y, y * 48271)

        # set geo for all those which depend on others.  Working left-to-right, top-to-bottom should be computable
        #  in a single pass.  (Also, top-to-bottom, left-to-right.)
        for y in range(self.height):
            for x in range(self.width):
                if (x, y) in (COORD_MOUTH, COORD_TARGET) or x == 0 or y == 0:
                    continue
                self.set_geo(x, y, self.get_erosion(x - 1, y) * self.get_erosion(x, y - 1))

    def _set_visualization_type_values(self) -> NoReturn:
        # set types (for visualization)
        for y in range(self.height):
            for x in range(self.width):
                self.set_type(x, y, self.get_erosion(x, y) % 3)

    def _compute_tool_map(self) -> NoReturn:
        """
        Since every square type can use two different tool types, computing where a given tool can be used may expose
         previously-unseen contiguous regions of travel without switching tools.  So, we create 3 maps from
         squares' region "type" values.
        """
        for y in range(self.height):
            for x in range(self.width):
                region_type = self.get_type(x, y)
                if region_type == TYPE_NARROW:
                    tools_allowed = TOOL_NEITHER + TOOL_TORCH
                elif region_type == TYPE_ROCKY:
                    tools_allowed = TOOL_CLIMB + TOOL_TORCH
                else:  # TYPE_WET
                    tools_allowed = TOOL_CLIMB + TOOL_NEITHER
                self.set_allowed_tools(x, y, tools_allowed)


    def _compute_all_values(self) -> NoReturn:
        self._compute_geo_values()
        self._set_visualization_type_values()
        self._compute_tool_map()


def must_change_tool(cur_tool: int, next_square_type: int) -> bool:
    return (next_square_type == TYPE_ROCKY and cur_tool == TOOL_NEITHER) \
        or (next_square_type == TYPE_NARROW and cur_tool == TOOL_CLIMB) \
        or (next_square_type == TYPE_WET and cur_tool == TOOL_TORCH)


def all_tools_except(cur_tool: int) -> Tuple[int, int]:
    if cur_tool == TOOL_CLIMB:
        return TOOL_NEITHER, TOOL_TORCH
    if cur_tool == TOOL_NEITHER:
        return TOOL_CLIMB, TOOL_TORCH
    return TOOL_CLIMB, TOOL_NEITHER



class PathFinder(object):
    def __init__(self, graph: EarthGraph):
        self.graph = graph

    def get_path_cost(self, current_tool: int, coord_sets: List[Tuple[int, int]]) -> int:
        region_type = self.graph.get_type(*coord_sets[0])
        tool_change_needed = must_change_tool(current_tool, region_type)

        # End of recursion
        if len(coord_sets) == 1:
            return 8 if tool_change_needed else 1

        if tool_change_needed:
            other_tools = all_tools_except(current_tool)
            # ISSUE: recursing like this for every possible tool-change takes far too long, as it needs 2^788 or so branches.
            #  We need a heuristic, which is ideally perfect at picking the lowest or tied-lowest cost, for which tool to pick.
            #   Should we do lookahead and whichever type we see more of next?  Or is there a lookahead algorithm that can tell how many switches are needed one way or another?
            #     Should we precompute a lookup table?
            # Or do we need to attack the pathing problem from some other angle, e.g. finding unified-tool paths first, then trying to path between them?
            #  Should we write this as a separate unit-testable piece?
            # return 8 + min(
            #     self.get_path_cost(other_tools[0], coord_sets[1:]),
            #     self.get_path_cost(other_tools[1], coord_sets[1:])
            # )
            #return 8 + self.get_path_cost(other_tools[0], coord_sets[1:])
            return 8 + self.get_path_cost(other_tools[1], coord_sets[1:])
        else:
            return 1 + self.get_path_cost(current_tool, coord_sets[1:])

    def get_path(self, coord_start: Tuple[int, int], coord_end: Tuple[int, int]) -> List[Tuple[int, int]]:
        """ A very simple implementation to get started. """
        coord_sets = [coord_start]
        while coord_sets[-1] != coord_end:
            if coord_sets[-1][0] < coord_end[0]:
                coord_sets.append((coord_sets[-1][0] + 1, coord_sets[-1][1]))
            elif coord_sets[-1][1] < coord_end[1]:
                coord_sets.append((coord_sets[-1][0], coord_sets[-1][1] + 1))
            else:
                coord_sets.append(coord_end)
        return coord_sets

    # TODO: define, describe, then implement an algorithm which draws curved lines from the origin to the target, passing through different sets of squares in an arc,
    #  and then finding all regions of allowed-tool-use underneath, and then finds the smallest number of overlapping different ones.




def print_graph_region_types(graph: EarthGraph) -> NoReturn:
    """
    Visualize the graph space as the region types.
    """
    for y, row in enumerate(graph.type_rows):
        charas = [DISPLAY_TYPES[terrain_type] for terrain_type in row]
        if y == 0:
            charas[0] = 'M'
        elif y == COORD_TARGET[1]:
            charas[COORD_TARGET[0]] = 'T'
        print(''.join(charas))


def print_graph_tool_types(graph: EarthGraph) -> NoReturn:
    """
    Visualize the graph space by allowed tool type.  This prints as three copies of the graph, one for each tool.
    """
    def print_graph_tool_type(test_tool_bit: int, tool_symbol: str) -> NoReturn:
        for y, row in enumerate(graph.allowed_tool_rows):
            charas = [tool_symbol if allowed_tools & test_tool_bit else ' ' for allowed_tools in row]
            if y == 0:
                charas[0] = 'M'
            elif y == COORD_TARGET[1]:
                charas[COORD_TARGET[0]] = 'T'
            print(''.join(charas))
        print()
    print_graph_tool_type(TOOL_CLIMB, 'c')
    print_graph_tool_type(TOOL_NEITHER, 'n')
    print_graph_tool_type(TOOL_TORCH, 't')


def run_main():
    graph = EarthGraph(COORD_TARGET[0] + 5, COORD_TARGET[1] + 5)
    print_graph_tool_types(graph)
    # NOTE: you can only leave the Mouth and enter the Target via the Torch-allowed layer.
    #pather = PathFinder(graph)
    #path1 = pather.get_path(COORD_MOUTH, COORD_TARGET)
    #cost = pather.get_path_cost(TOOL_TORCH, path1)
    #print(f'Test path with {len(path1)} nodes has cost {cost}.')

    #print(f'Risk level to target: {graph.get_risk_level(*COORD_TARGET)}')


if __name__ == '__main__':
    run_main()

# # Note: we will likely have to compute paths for an unknown distance beyond the target.  Can we guesstimate the
# #  maximum theoretical boundary that a path could be more efficient than worst-casing taxicab-directly?  This would
# #  give a secure bound for how much graph we need to make.
# # Attempt at first algorithm:
# """
# * as we compute the cost of paths, using some form of every-possible-path checking, we need to have a boundary of the
#   cheapest path we know, for far, we can use; and any possible computation which will theoretically exceed this cost
#   gets abandoned ASAP.
# """
# """
# * narrow: torch or neither
# * rocky: climb or torch
# * wet: climb or neither
#
# N <-> R: must have torch
# N <-> W: must have neither
# W <-> R: must have climb
# """