### Do not run; see answer_2b.py.

from typing import List, NoReturn, Optional

# REAL input values:
CAVE_DEPTH = 11820
COORD_TARGET = (7, 782)
COORD_MOUTH = (0, 0)

#TEST input values:
#CAVE_DEPTH = 510
#COORD_TARGET = (10, 10)
#COORD_MOUTH = (0, 0)

NOT_COMPUTED = -1
TYPE_ROCKY = 0
TYPE_WET = 1
TYPE_NARROW = 2

DISPLAY_TYPES = ('.', '=', '|')

def cost_for_shift(starting_type: int, ending_type: int) -> int:
"""
Documentation about moving from one type to another:
### Note: fatal flaw in algorithm: cost to switch from one square to another has hysteresis; it's not constant based
###  just on the source and target square types!  See answer_3.py.
* narrow: torch or neither
* rocky: climb or torch
* wet: climb or neither

N <-> R: must have torch
N <-> W: must have neither
W <-> R: must have climb
"""


class SquareEntryCost(object):
    """ Cost to cross the boundary INTO this square, headed from any direction. """
    def __init__(self, up_cost: int, right_cost: int, down_cost: int, left_cost: int):
        self.up_cost = up_cost
        self.right_cost = right_cost
        self.down_cost = down_cost
        self.left_cost = left_cost


def erosion_for_geo(geo_index: int) -> int:
    """ Given a geological index, return the erosion level. """
    return (geo_index + CAVE_DEPTH) % 20183


class EarthGraph(object):
    def __init__(self, width: int, height: int):
        self.width = width
        self.height = height
        self.geo_rows = []  # type: List[List[int]]
        self.erosion_rows = []  # type: List[List[int]]
        self.type_rows = []  # type: List[List[int]]
        self.entry_cost_rows = []  # type: List[List[Optional[SquareEntryCost]]]
        self.best_cost_to_mouth_rows = []  # type: List[List[int]]
        self.best_cost_to_target_rows = []  # type: List[List[int]]

        for i in range(height):
            self.geo_rows.append([NOT_COMPUTED] * width)
            self.erosion_rows.append([NOT_COMPUTED] * width)
            self.type_rows.append([NOT_COMPUTED] * width)
            self.entry_cost_rows.append([None] * width)
            self.best_cost_to_mouth_rows.append([NOT_COMPUTED] * width)
            self.best_cost_to_target_rows.append([NOT_COMPUTED] * width)
        self._compute_all_values()

    def set_geo(self, x: int, y: int, value: int) -> NoReturn:
        self.geo_rows[y][x] = value
        self.set_erosion(x, y, erosion_for_geo(value))

    def set_erosion(self, x: int, y: int, value: int) -> NoReturn:
        self.erosion_rows[y][x] = value

    def get_erosion(self, x: int, y: int) -> int:
        return self.erosion_rows[y][x]

    def get_type(self, x: int, y: int) -> int:
        return self.type_rows[y][x]

    def set_type(self, x: int, y: int, value: int) -> NoReturn:
        self.type_rows[y][x] = value

    def get_risk_level(self, max_x: int, max_y: int) -> int:
        level = 0
        for y in range(max_y + 1):
            for x in range(max_x + 1):
                level += self.get_type(x, y)
        return level

    def _compute_geo_values(self) -> NoReturn:
        # set geo for known values
        self.set_geo(*COORD_MOUTH, 0)
        self.set_geo(*COORD_TARGET, 0)

        # set geo for the left and top borders, which are directly computable.
        for x in range(self.width):
            if x != 0:
                self.set_geo(x, 0, x * 16807)
        for y in range(self.height):
            if y != 0:
                self.set_geo(0, y, y * 48271)

        # set geo for all those which depend on others.  Working left-to-right, top-to-bottom should be computable
        #  in a single pass.  (Also, top-to-bottom, left-to-right.)
        for y in range(self.height):
            for x in range(self.width):
                if (x, y) in (COORD_MOUTH, COORD_TARGET) or x == 0 or y == 0:
                    continue
                self.set_geo(x, y, self.get_erosion(x - 1, y) * self.get_erosion(x, y - 1))

    def _set_visualization_type_values(self) -> NoReturn:
        # set types (for visualization)
        for y in range(self.height):
            for x in range(self.width):
                self.set_type(x, y, self.get_erosion(x, y) % 3)

    def _compute_entry_values(self) -> NoReturn:
        max_x = self.width - 1
        max_y = self.height - 1
        for y in range(self.height):
            for x in range(self.width):
                up_cost = -1
                right_cost = -1
                down_cost = -1
                left_cost = -1
                if y > 0:
                    down_cost = cost_for_shift()
                if y < max_y:
                    up_cost = cost_for_shift()
                if x > 0:
                    right_cost = cost_for_shift()
                if x < max_x:
                    left_cost = cost_for_shift()

                self.set_type(x, y, self.get_erosion(x, y) % 3)

    def _compute_best_mouthward_path_cost_values(self) -> NoReturn:
        pass

    def _compute_best_targetward_path_cost_values(self) -> NoReturn:
        pass

    # TODO: method which gets the cost of the best route from the target to the mouth.
    # Note that at this point we don't have an easy way to assert that this route is so far within our computed area
    #  that we definitely didn't miss a better route, except with either brute-force "big enough" or fancy math.

    def _compute_all_values(self) -> NoReturn:
        self._compute_geo_values()
        #self._set_visualization_type_values()
        self._compute_entry_values()
        self._compute_best_mouthward_path_cost_values()
        self._compute_best_targetward_path_cost_values()





def print_graph(graph: EarthGraph) -> NoReturn:
    for y, row in enumerate(graph.type_rows):
        charas = [DISPLAY_TYPES[terrain_type] for terrain_type in row]
        if y == 0:
            charas[0] = 'M'
        elif y == COORD_TARGET[1]:
            charas[COORD_TARGET[0]] = 'T'
        print(''.join(charas))


def run_main():
    graph = EarthGraph(COORD_TARGET[0] + 1, COORD_TARGET[1] + 1)



    #print(f'Risk level to target: {graph.get_risk_level(*COORD_TARGET)}')


if __name__ == '__main__':
    raise Exception('Use answer_2b.py instead; answer_2a.py contains a fatal algorithm flaw and is abandoned.')
    #run_main()

# Note: we will likely have to compute paths for an unknown distance beyond the target.  Can we guesstimate the
#  maximum theoretical boundary that a path could be more efficient than worst-casing taxicab-directly?  This would
#  give a secure bound for how much graph we need to make.
# Attempt at first algorithm:
"""
  0. compute all edge costs in the graph and store them.  (Edge costs for paths are free of hysteresis; they're always true for each edge.) !!! NOT TRUE; algorithm won't work.
  1. for any point (K, L) on a square grid there are 2^((K-1) * (L-1)) taxicab-shortest paths to (0, 0).
    (For our solution, there CAN be other paths outside of this set of paths which may be "cheapest", but we're ignoring those to begin with.)   
    To solve all of these from end to end would be 2 ^ (7 * 782), which is clearly impossible.
  2. all non-shortest paths outside the (0, 0, K, L) box will add 2 length for every 1 square they go out of the way.
    A. every non-shortest path is a (presumably short) path out of the way followed by an equal-length path the correct
    way in order to enter a point on one of the shortest-paths we've already computed.  It may be possible for us to
    solve for the cheapest path from each point along the (K, L) box edges, so for outside-the-box paths we only
    need to additionally compute the cost of the shortest away-and-back paths to each edge point.
  3. we should determine the worst-case direct path cost.  This would be (K + L ) * 8.  This is our starting minimum
    value for testing each path to find a cheaper one.  (The value 1 billion would also work, probably.)
  4. Q: should we start at (0, 0) and compute the minimum cost to get there for every square at distance 1, then compute
    the cost to get to each distance 1 square from the distances 2 squares, and so on?  We would store the running sum,
    so we never have to compute any square-hop cost twice.  Every square can directly reach 2 squares, so there should
    only be 2 * K + L computations - about 11,000.  This is far more doable.  Then we can compute the cost from (K, L)
    to the edge boxes, add to their home costs, and store those separately.  This would enable us to look up the 
    cheapest path from (K, L) to (0, 0), and from there we just have to solve for cheaper paths that go outside the
    bottom or right sides.
  5. After computing all _lowest_ cost paths to reach (0, 0) from every point in some rectangle (K + A, L + B), we
    could next compute all costs to reach the Target point (Tx, Ty) from every point within some distance that target.
    Every point we compute both costs, the sum of the costs is the full lowest route cost through that point.
    Q: how do we define / bound the points we want to best-path-cost to the Target?
    A: one way is to use the line (X + Y = Tx + Ty) as a starting point; this is bounded by x = 0 and y = 0 and creates
       two "hemispheres": toward (0, 0) and away.  On the "toward" side, that line is actually enough, since the prior
       computation of the (0, 0, K, L) box will pass that line; the overlap on that line should give us the lowest cost
       to go taxicab-directly from the Target to the Mouth. 
       Then on the "away" side, we can compute the line (X + Y = Tx + Ty + C) repeatedly (incrementing C) until we have
       "enough" lines, which could be defined as when the lowest path cost to the Target from every square in one line
       is more than the lowest taxicab-direct cost from the Target to the Mouth.  If computation is cheap, this may
       be fast enough.  A less-computational option is for us to check the combined cost of the "away" paths with the
       origin path costs at every square in each line right after we compute it, and as soon as the smallest of those
       exceeds the lowest taxicab-direct cost, we know any possible solution we could want is in there.
    A2: another way to bound the region for which we need to compute the cost of the shortest path to the Target is to
        compute the taxicab-direct paths first (this is bounded; above) and then best-case the cost of an away path,
        which would cost 1 per square traveled; we then compute the lines (X + Y = Tx + Ty + C) until C is greater than
        half the difference between the best taxicab-direct path and the cheapest path from any square on the bottom or
        right edge of the (0, 0, K, L) box.  No cheaper "away, then over/up, then back" path can exist outside that
        boundary, and by first computing the best taxicab-direct path, we avoid the cost of the next bounding strategy.
    A3: naively, we could just worst-case all direct routes then best-case the longest away route.  This would be a
        cost of 8-per-distance for the direct routes except a cost of 1-per-distance along the left or top edge of
        the (0, 0, K, L) box.  Orienting so K < L, direct route worst-case cost is (8 * (L - K) + 1), which assumes
        there's a perfect-cost path along the (0, 0, K, L) box furthest from the Target.  The perfect "away" path to
        get there will cost (L - K) to go parallel to the far box side, then L to get to the box side, then (L - K)
        again for the length of the box side to (0, 0).  (Due to taxicab math, all other paths inside this bounding-box
        case will be included inside it, and since we already said our bounding path has the best-case cost per 
        distance, no longer path can have a lower cost.)  This means (3L - 2K)     
"""
"""
* narrow: torch or neither
* rocky: climb or torch
* wet: climb or neither

N <-> R: must have torch
N <-> W: must have neither
W <-> R: must have climb
"""