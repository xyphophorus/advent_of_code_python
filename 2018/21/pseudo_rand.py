from collections import deque
from typing import NoReturn

MAGIC_MULTIPLICAND = 65899


def add_17th_bit(in_int: int) -> int:
    return 65536 | in_int


def crop_8(in_int: int) -> int:
    return 255 & in_int


def crop_24(in_int: int) -> int:
    return 16777215 & in_int


class Machine(object):
    def __init__(self, initial_reg_1: int, initial_reg_2: int):
        self.reg_1 = initial_reg_1
        self.initial_reg_2 = initial_reg_2
        self.reg_2 = initial_reg_2
        self.seen_checkable_r2_values = set()
        self.last_checkable_r2_value = -1

    def note_checkable_r2(self, in_value: int) -> None:
        if in_value in self.seen_checkable_r2_values:
            print(f'First repeat of an r2 value seen - value = {in_value}.')
            print(f'Most recent prior r2 value: {self.last_checkable_r2_value}.')
        self.seen_checkable_r2_values.add(in_value)
        self.last_checkable_r2_value = in_value

    def permute(self) -> NoReturn:
        r2 = self.reg_2
        r1 = self.reg_1 // 256

        addend = crop_8(r1)
        r2 = crop_24(crop_24(r2 + addend) * MAGIC_MULTIPLICAND)

        # Per the original algorithm, the r2 value here are the only "valid" ones for checking r0 against.
        #  So we want to find the last non-repeating of this value.
        if r1 < 256:
            #print(f'r1 < 256; r1={r1}, r2={r2} but is being re-formed')
            self.note_checkable_r2(r2)
            r1 = add_17th_bit(r2)
            r2 = self.initial_reg_2
            addend = crop_8(r1)
            r2 = crop_24(crop_24(r2 + addend) * MAGIC_MULTIPLICAND)

        self.reg_2 = r2
        self.reg_1 = r1


if __name__ == '__main__':
    cur_rep = 0
    max_reps = -1
    machine = Machine(65536 * 256, 1250634)
    while cur_rep < max_reps or max_reps < 0:
        print(f'Rep {cur_rep}, r1={machine.reg_1}, r2={machine.reg_2}')
        machine.permute()
        cur_rep += 1

# The first r2 to repeat is 3308537,
#  and the prior value (so, the last non-repeating r2 value) is 13599657.
#  Therefore, 13599657 will be my first guess.
#   This was the correct answer!