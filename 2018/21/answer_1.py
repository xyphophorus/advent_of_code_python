from typing import List, NoReturn
from collections import deque

class OpCode(object):
    ADD_IMM = 'addi'
    ADD_REG = 'addr'
    BINARY_AND_IMM = 'bani'
    BINARY_AND_REG = 'banr'
    BINARY_OR_IMM = 'bori'
    BINARY_OR_REG = 'borr'
    EQUAL_REG_IMM = 'eqri'
    EQUAL_REG_REG = 'eqrr'
    GREATER_IMM_REG = 'gtir'
    GREATER_REG_REG = 'gtrr'
    MUL_REG = 'mulr'
    MUL_IMM = 'muli'
    SET_IMM = 'seti'
    SET_REGISTER = 'setr'


def format_registers(in_registers: List[int]) -> str:
    return ''.join([
        f'{register: 10}' for register in in_registers
        ])


class Instruction(object):
    def __init__(self):
        self.op = '(NOT YET SET)'
        self.input_a = -1
        self.input_b = -1
        self.output_c = -1

    @staticmethod
    def load_from_str(str_form: str) -> 'Instruction':
        new_instance = Instruction()
        op, str_input_a, str_input_b, str_output_c = str_form.split()
        new_instance.op = op
        new_instance.input_a = int(str_input_a)
        new_instance.input_b = int(str_input_b)
        new_instance.output_c = int(str_output_c)
        return new_instance

    def __repr__(self) -> str:
        return f'Op {self.op}, in_A={self.input_a}, in_B={self.input_b}, out_C={self.output_c}'


class InvalidInstructionCounter(Exception):
    pass


class RuntimeExceeded(Exception):
    pass


class Machine(object):
    def __init__(self, instructions: List[Instruction], starting_reg_0_val: int):
        self.registers = [starting_reg_0_val, 0, 0, 0, 0, 0]
        self.shadow_instruction_counter_register = 5  # specified by the real_input.txt
        self.instruction_counter = 0
        self.instructions = instructions

    def run_instruction(self, instruction: Instruction) -> NoReturn:
        ix = instruction
        if ix.op == OpCode.ADD_IMM:
            self.registers[ix.output_c] = self.registers[ix.input_a] + ix.input_b
        elif ix.op == OpCode.ADD_REG:
            self.registers[ix.output_c] = self.registers[ix.input_a] + self.registers[ix.input_b]
        elif ix.op == OpCode.BINARY_AND_IMM:
            self.registers[ix.output_c] = self.registers[ix.input_a] & ix.input_b
        elif ix.op == OpCode.BINARY_AND_REG:
            self.registers[ix.output_c] = self.registers[ix.input_a] & self.registers[ix.input_b]
        elif ix.op == OpCode.BINARY_OR_IMM:
            self.registers[ix.output_c] = self.registers[ix.input_a] | ix.input_b
        elif ix.op == OpCode.BINARY_OR_REG:
            self.registers[ix.output_c] = self.registers[ix.input_a] | self.registers[ix.input_b]
        elif ix.op == OpCode.EQUAL_REG_IMM:
            self.registers[ix.output_c] = 1 if self.registers[ix.input_a] == ix.input_b else 0
        elif ix.op == OpCode.EQUAL_REG_REG:
            self.registers[ix.output_c] = 1 if self.registers[ix.input_a] == self.registers[ix.input_b] else 0
        elif ix.op == OpCode.GREATER_IMM_REG:
            self.registers[ix.output_c] = 1 if ix.input_a > self.registers[ix.input_b] else 0
        elif ix.op == OpCode.GREATER_REG_REG:
            self.registers[ix.output_c] = 1 if self.registers[ix.input_a] > self.registers[ix.input_b] else 0
        elif ix.op == OpCode.MUL_IMM:
            self.registers[ix.output_c] = self.registers[ix.input_a] * ix.input_b
        elif ix.op == OpCode.MUL_REG:
            self.registers[ix.output_c] = self.registers[ix.input_a] * self.registers[ix.input_b]
        elif ix.op == OpCode.SET_IMM:
            self.registers[ix.output_c] = ix.input_a
        elif ix.op == OpCode.SET_REGISTER:
            self.registers[ix.output_c] = self.registers[ix.input_a]
        else:
            raise Exception(f'No case for opcode "{ix.op}".')

    def step(self):
        # Day 19 thing: copy IC down to shadow-register
        self.registers[self.shadow_instruction_counter_register] = self.instruction_counter

        if 0 > self.instruction_counter or self.instruction_counter > len(self.instructions) - 1:
            print(f'Invalid instruction counter: {self.instruction_counter}.')
            raise InvalidInstructionCounter()

        if self.instruction_counter == 26:

            break_point = 'kittens'
        self.run_instruction(self.instructions[self.instruction_counter])

        # Day 19 thing: copy shadow-register back up to IC after running opcode.
        self.instruction_counter = self.registers[self.shadow_instruction_counter_register]
        # Increment IC _after_ running opcode AND, of course, after copying the shadow-register back up
        self.instruction_counter += 1


def run_program_awhile(instructions: List[Instruction], starting_reg_0_val: int, max_allowed_steps: int) -> NoReturn:
    machine = Machine(instructions, starting_reg_0_val)
    steps_run = 0
    last_instruction_counter_values = deque(maxlen=10)

    try:
        while True:
            last_instruction_counter_values.append(machine.instruction_counter)
            #if machine.instruction_counter == 27:
            #    for ic in last_instruction_counter_values:
            #        print(f'PC0: {ic}')
            #    exit()
            machine.step()
            #print(f'PC0: {machine.instruction_counter}\t[{format_registers(machine.registers)}]')
            steps_run += 1

            if machine.instruction_counter == 13:
                #print(f'PC0 == 28 after {steps_run} steps completed.')
                print(f'PC0: {machine.instruction_counter}\t[{format_registers(machine.registers)}]')
                #if steps_run % 10000 == 0:
            #    print(f'Steps: {steps_run}')
            if steps_run >= max_allowed_steps:
                raise RuntimeExceeded
    except InvalidInstructionCounter:
        print('Machine execution concluded.')
        print(f'Completed {steps_run} steps before invalid instruction counter was detected.')
    except RuntimeExceeded:
        print(f'Machine was still running beyond {max_allowed_steps} steps, so it was aborted; ' +
              f'reg_0 initial input was {starting_reg_0_val}.')


def run_main():
    instructions = [Instruction.load_from_str(line.strip())
                    for line in open('real_input.txt', 'r').readlines()
                    if line.strip() and not line.startswith('#')]
    #print(instructions)
    #for i in range(100):
    #    starting_reg_0_val = i
    #    run_program_awhile(instructions, starting_reg_0_val, 1000 * 1000)
    # For PART-1, this reg_0 initial value (10780777) is the correct answer.
    #run_program_awhile(instructions, 10780777, 1000 * 1000)

    # for PART-2, as part of our experimentation we now want to run the program for "a while" and keep checking its reg_2 value over time.
    run_program_awhile(instructions, 0, 1000 * 1000 * 1000)


if __name__ == '__main__':
    run_main()
